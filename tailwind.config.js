/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    './dist/index.html',
    './src/**/*.{html,js}'
  ],
  theme: {
    extend: {
      colors: {
        primary: '#DC294F',
        'primary-dark': '#b02140',
        'primary-light': '#FDDAE0',
        title: '#0E0E0E',
        subtitle: '#AFAFAF',
        'subtitle-two': '#828282',
        'subtitle-three': '#C3C3C3',
        disabled: '#D0D0D0',
        placeholder: '#D8D8D8',
        'placeholder-dark': '#9B9B9B',
        border: '#EEEEEE',
        'border-two': '#ECECEC',
        'border-three': '#F0F0F0',
        ads: '#5A5A5A',
      },
      zIndex: {
        1: 1,
      }
    },
  },
  plugins: [],
}
