# Female Daily Test - Point A - Demaspira Aulia

This project is for Female Daily Test Point A: Slicing an Image

Author: Demaspira Aulia

Check the [live demo here](https://female-daily-test-point-a-demaspira-aulia.vercel.app/)

## Prequisites

You will need Node.js and NPM to use this project.

You windows user, you can check for the installation file [here](https://nodejs.org/).

And for linux user, you can check for the installation file [here](https://nodejs.org/) or search for your spesific distro in the web.

## Getting Started

First, install all required dependecies:
```
npm install
```

and then, start tailwind build process:
```
npm run watch-css
```

after that, run this command on another terminal to serve the html file:
```
npm run start
```

Open [http://localhost:3000](http://localhost:3000) with your browser.

## Developing

You can start developing by editing the index.html in the dist folder.

If you need to change anything on the css, you can edit the main.css file on src/css folder.

## Learn More

You can learn more about Tailwindcss here:

- [Tailwindcss](https://tailwindcss.com/docs/installation)
